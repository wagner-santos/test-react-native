## Back-End

- Node
- Express

## App

- React-native
- Axios
- Redux
- Redux-saga
- React-navigation
- Styled-components

## Run Back-End

- clone repository

```
  - cd node-app-docker
  - docker-compose up -d
  - cd ..
```

## Run App

```
  - cd client
  - yarn or npm install
  - yarn ios or yarn android
```

## Questions

```
1. What is your experience with the Javascript language? Name the main project in which you used this language, the main frameworks involved and the architecture used.

  Since when I started programming I work with javascript. I worked with react native app for Farmazon with redux, react-router-flux, react-native-elements.

2. Do you have experience with another backend language?

  I didn't really work with any language but I know node, php, java, python, c#, vba.

3. What is your experience with frontend technologies such as AngularJs, ReactJS?

  I have 1 year of experience with React and I know a little of AngularJS and VueJS

4. What is your experience with mobile application development?

  I worked on apps built with react native. Two of which were published in stores.

5. What is your experience with virtualizing environments (eg Docker)?

  I know a little and I am learning docker.

6. What is your role in the teams of the last projects where you worked? How big are these teams?

  I worked as a developer with teams of 5 members.
```
